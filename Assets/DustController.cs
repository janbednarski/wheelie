﻿using System.Collections.Generic;
using UnityEngine;
public class DustController : MonoBehaviour {
    public GameObject object_;
    [Range(0f, 0.2f)]
    public float particlesMultiplier = 0.2f;
    public
    List<GameObject> dustPlanes = new List<GameObject>();
	
	void Start () 
    {
        Debug.Assert(object_ != null);
	}

    bool isInsideOfAnyOfDustPlanes()
    {
        var objectPosition = object_.GetComponent<Collider>().bounds;
        foreach (var dustPlane in dustPlanes)
        {
            if(dustPlane.GetComponent<Collider>().bounds.Intersects(objectPosition))
            {
                return true;
            }
        }

        return false;
    }
	
	void FixedUpdate () 
    {
        if (!isInsideOfAnyOfDustPlanes())
        {
            return;
        }

        var particles = GetComponent<ParticleSystem>();
        var shapeModule = particles.shape;
        shapeModule.position = new Vector3(object_.transform.position.x,
                                           object_.transform.position.y - object_.transform.localScale.y / 2f, 
                                           object_.transform.position.z);
        
        if (object_.GetComponent<Rigidbody>().velocity != Vector3.zero)
        {
            var velocityMgn = object_.GetComponent<Rigidbody>().velocity.sqrMagnitude;
            var particlesf =  velocityMgn * (particlesMultiplier);
            var particlesCount = Mathf.RoundToInt(particlesf);
            GetComponent<ParticleSystem>().Emit(particlesCount);
        }
	}
}
