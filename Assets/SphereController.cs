﻿using UnityEngine;

public class SphereController : MonoBehaviour 
{
    public float speed = 15.0f;
    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(movement * speed);

        //Vector3 movement = Vector3.zero;
        //if (Input.GetKey(KeyCode.LeftArrow))
        //{
        //    movement += Vector3.left * speed;
        //}
        //if (Input.GetKey(KeyCode.RightArrow))
        //{
        //    movement += Vector3.right * speed;
        //}
        //if (Input.GetKey(KeyCode.UpArrow))
        //{
        //    movement += Vector3.forward * speed;
        //}
        //if (Input.GetKey(KeyCode.DownArrow))
        //{
        //    movement += Vector3.back * speed;
        //}
        //rb.AddForce(movement);
    }
}
