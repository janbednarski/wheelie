﻿using UnityEngine;

public class WallController : MonoBehaviour {
    [Range(1,20)]
    public uint rows = 10;
    [Range(1, 20)]
    public uint width = 20;
    GameObject brickPrefab_;
    float brickWidth_ = 1f;
    const float xOffsetToAvoidRbExplosion = 0.1f;
    const float yOffsetToAvoidRbExplosion = 0.2f;

	void Start () {
        brickPrefab_ = Resources.Load("Brick") as GameObject;
        Debug.Assert(brickPrefab_, "Failed to load brick prefab");
        brickWidth_ = brickPrefab_.transform.localScale.x / 2f; 

        for (uint x = 0; x < width; ++x)
        {
            for (uint y = 0; y < rows; ++y)
            {
                var place = getPlace(x, y);
                var brick_ = Instantiate(brickPrefab_, place, Quaternion.identity, transform);
            }
        }
	}

    Vector3 getPlace(uint x, uint y)
    {
        var brickScale = brickPrefab_.transform;
        var res = new Vector3(transform.position.x + brickScale.localScale.x * x + xOffsetToAvoidRbExplosion,
                              transform.position.y + brickScale.localScale.y * y + yOffsetToAvoidRbExplosion, 
                              transform.position.z);
        if(y%2 == 0){
            res.x += brickWidth_;
        }
        return res;
    }
}
